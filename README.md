Create your own network probe: a TAP
===================

This project gather all information about buying and building your own tap.

Many information are available on the wiki.
fr: https://framagit.org/network-probe/tap/wikis/home
en: https://framagit.org/network-probe/tap/-/wikis/Home-(english)

In this repository, the folder *galery* contains screenshots and pictures about the devices.
The folder *docs* contains files and notes about the devices.

# License: BSD 3-Clause 

https://framagit.org/pierrick/mqtt-sn/-/raw/master/LICENSE

# Contributing

Do not hesitate to improve to this program. Feel free to send PR or contact me to send comments. You are welcome to fork this project also ;)

# Badges

[![License](https://img.shields.io/badge/License-BSD%203--Clause-green.svg)](https://opensource.org/licenses/BSD-3-Clause)
